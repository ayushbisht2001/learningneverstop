## [MVC - Model View Controller Architecture](https://www.guru99.com/mvc-tutorial.html)

- The Model-View-Controller (MVC) framework is an architectural pattern that separates an application into three main logical components Model, View, and Controller.
- MVC separates the business logic and presentation layer from each other
- With this architecture, it is quite easy to maintain application code and extend them. It also reduces the complexity of application development

Three important MVC components are:

- **Model** : It includes all the data and its related logic
- **View** : Present data to the user
- **Controller** : An interface between Model and View components and  handles user interaction

![Alt text](mvc.png)


**Examples**

- View= You
- Waiter= Controller
- Cook= Model
- Refrigerator= Data
