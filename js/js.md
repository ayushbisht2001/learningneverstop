### <h1 align = "center"  >**JS Notes**</h1>

<style >
  *{
    margin : 0px;
    box-sizing : border-box;
  }
table {
font-family: arial, sans-serif;
border-collapse: collapse;
width: 100%;
}

td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}

.flex{
  display : flex;
  flex-direction : column;
  max-width : 400px;
  height : auto;
  padding : 10px;
  margin : 5px;
  overflow : hidden;
  place-items : center;
}
.box{
  width : 100%;
  padding : 5px;
  margin : 5px;
  min-height : 100px;
}
.pre{
  margin: 0px;
  padding : 0px;
}

</style>

## **Content**
- ### **[Fundamentals](#fundamentals)**
  - 
- ### **[Functions](#functions)**
  - 
- ### **[Oops](#oops)**
  - 
- ### **[Concepts](#concepts)**
  - [Prototype](#prototype)


# J S ? 😩 
  
   -  [It is a loosely typed and dynamic language.](#loosely_typed)
# <h2 name = "fundamentals"> **Fundamentals** </h2>


### Data Types
  JavaScript is a <a id = "loosely_typed">loosely typed</a> and dynamic language. Becoz Variables in JavaScript are not directly associated with any particular value type, and any variable can be assigned (and re-assigned) values of all types:

  ```js

    let foo = 42;    // foo is now a number
    foo     = 'bar'; // foo is now a string
    foo     = true;  // foo is now a boolean
  ```

  ### Primitive Datatypes
  Except **Object** , all preimitive datatypes are immutable
    
- Boolean
- null
- Number

    ```js
      /*
          double-precision 64 bit number
          range = 2e-1074 to 2e1024  ( Number.MIN_VALUE - Number.MAX_VALUE)

          Notes : 

          n < Number.MIN_VALUE = -Infinity ( implicitly converted)
          n > Number.MAX_VALUE = +Infinity ( implicitly converted)
      */

        > 42 / +0
          Infinity
        > 42 / -0
          -Infinity

    ```
- [**BigInt**](https://golb.hplar.ch/2018/09/javascript-bigint.html#:~:text=BigInt%20is%20a%20new%20JavaScript,values%20up%20to%202%5E53.)

  for numeric values greater than Numer.MAX_VALUE. Generally it is used for showing cryptographic value, prices at the client side with high accuracy.

  ```js
    // Any number can be converted to BigInt by appending 'n' to it

    var x = 120n;

    // BigInt doesn't work with Numeric

    x = x + 10  // Uncaught TypeError: Cannot convert a BigInt value to a number
    x = x + 10n // Correct

    10 == 10n
    // true

    10 === 10n
    // false


    /*
        BigInt can be converted to other types using Numeric , parseInt, parseFloat method, but in case of out of range infinity will be return for that data type
    */

    const x = BigInt(Numeric.MAX_VALUE + 2)

    Numeric(x) // +infinity

    BigInt(10) === 10n; // true
    BigInt('100') === 100n;  // true
    BigInt(23.5)// RangeError: 


  ``` 

- [String](https://www.w3schools.com/jsref/jsref_obj_string.asp) 
  
  strings in JS are immutable ( we can't modify it once created)

  ```js

    let x = "1234"
    x[0] = '2' //  no change no error
    let y = "0024"

    x.length // 4

    /*
      Extracting String 
        range :  [start, end )

      1 - slicing in string
          str.slice(start, end) 

      2 - str.substring(start, end)
          similar to slice , but doesn't accept negative indices
      3 - str.substr(start, length)
    
    */
    console.log(str.slice(0, 2))  // "12"

    /*
      starts from third character from last postion till first character from last.
    */
    console.log(str.slice(-3, -2)) // "23"
    
    // gives 3 characters from last
    console.log(str.slice(-3)) // "234"  

    console.log(str.substring(0,3)) // "123"    


    // Removing whitespace from start and end of string
    str.trim()

    // concatenation
    console.log(x.concat("+", y)) // 1234+0024


    /*
      Convert String to array 

      - using split method
    */

      let str = "1,2,3,4,5"
      str.split(",")  // ['1', '2', '4', '4', '5']
      

  ```

-  Object

    object in js are mutable. However an object can be made immutable through freeze method
    
     [ more >>](#object_in_js)

# <h2 name = "fundamentals"> **[Execution Context](https://www.javascripttutorial.net/javascript-execution-context/)** </h2>

It is nothing but the environment where JS code are executed.

If env is created in global context it is called as <i style="color:yellow"> Global Execution Context </i> (GEC). 

If it is created in any function context then <i style="color:yellow"> Function Execution Context </i> (FEC).


  
  ### Phase in Execution context
  1. Creation Phase - <i style="color:yellow">"**Declaration phase**"</i>
  2. Execution Phase - <i style="color:yellow">" **Initialization Phase** "</i>
   
  ### **Creation Phase**
  


  During this phase, JS engine do the following tasks

  ### **For Global Context**

<center>
  
  <img width = "240px" height = "250px" src="assets/execution-context.png" />

</center>
  
  


  1. create global object
  2. Create a this object binding which points to the global object above.

  3. setup memory heaps for storing variables and function references   <i style="color:magenta">**--> declaration of variable name and function in memory**</i>
  4. Store the function declarations in the memory heap and variables within the global execution context with the initial values as <i style = "color:yellow">**undefined**</i> 
  


  ### **For Function Context**
  

<center>
  
  <img width = "250px" height = "360px" src="assets/function-executiion-context.png" />

</center>


   1. function argument will be created as global objects
   2. Rest of the thing are same as above
   
### **Execution Phase**

* During the execution phase, the JavaScript engine executes the code line by line. In this phase, it assigns values to variables and executes the function calls 
* For every function call the execution context is created for them aswell.
## **Working of Execution Context**


```js
  var a = 10
  var b = 20

  function fun(  name ){
    var c = 3
    return name
  }

  fun("ayush")

  function fun2( ) {

    var c = 2

    function  fun3(){
      return 3;
    }

    return fun3()

  }

  fun2()



```
<center>
  <h3 style="color : red" >Global execution context </h3>

  <table
  >
  <tr>
    <th>Memory / Variable Env. </th>
    <th>Code / Thread Execution</th>
  </tr>
  <tr>
    <td>this = window</td>
      <td>line a = 10 get executed and undefined is replaced with 10</td>



  </tr>
  <tr>
    <td>a = undefine</td>
    <td>b is initialized</td>

  </tr>
  <tr>
    <td>b  = undefined</td>
    <td> 

  fun() called and new execution context created

  <h3 style="color : yellow" >Fun execution context </h3>
  <table
  >
  <tr>
    <th>Memory  </th>
    <th>Code </th>
  </tr>
  <tr>
    <td>this = window</td>
    <td></td>


  </tr>
  <tr>
    <td>name = ayush</td>
    <td>line c = 3 get executed and undefined is replaced with 3</td>

  </tr>
  <tr>
    <td>c = 3</td>
    <td>name is return</td>
    

  </tr>
 
  
</table>
  <h4 style="color : yellow" >Fun execution context get terminated / popped from call stack </h4>
  
  </td>

  </tr>
  <tr>
    <td>fun =  function fun(  name ){
    var c = 3
    return name
    }
    </td>

  <td>
    fun2() is called , push to call stack and new execution context created
    <h3 style="color : magenta" >Fun2 execution context </h3>
     <table
  >
  <tr>
    <th>Memory  </th>
    <th>Code </th>
  </tr>
  <tr>
    <td>this = window</td>
    <td></td>


  </tr>
  <tr>
    <td>c = 2</td>
    <td>line c = 2 get executed and undefined is replaced with 2</td>

  </tr>
  <tr>
    <td>fun3 = function  fun3(){
      return 3;
    }</td>
    <td>
    fun3() is called , push to call stack and new execution context created
    <h3 style="color : aqua" >Fun2 execution context </h3>
     <table  >
  <tr>
    <th>Memory  </th>
    <th>Code </th>
  </tr>
  <tr>
    <td>this = window</td>
    <td>return 3</td>
  </tr>
</table>
    <h3 style="color : aqua" >Fun3 pop out from call stack </h3>

</td>
</tr>
</table>
    <h3 style="color : magenta" >Fun2 pop out from call stack </h3>

</td>
</tr>
  <tr>
    <td>fun2 = function fun2( ) 
      {
          var c = 2 
          function  fun3(){
          return 3;
          }
        return fun3()
    }
  </td>

  </tr>
  <tr>
  </tr>
</table>

</center>

## **Scope in JS**

Scope is something that manages the accessibility of variables.

Scopes can also be layered in a hierarchy, so that child scopes have access to parent scopes, but not vice versa

  **Block Scope**
    
  * code block define a block scope for *let and const variable* only.

  ```js
  {
    let a = 10;
    console.log(a)
  }
    console.log(a) //  reference error

  if(true){
    const x = 10;
    var v = 20; // can be accessible outside

  }

  console.log(x) // error
  console.log(v) // 20 : code block can't create scope for var variables.
  ```

  **Function Scope**
    
  * function create scope for  *let, const and var variables and even function aswell*

  ```js

    function fun(){
      
      var a = 20;
      
      console.log(x) // reference error
      
      function inner(){
        var x = 10;
        console.log(a) // 20
      }

      inner(); 
    }

    console.log(a) // error
    fun()
    inner() // reference error


  ``` 

**Module scope**

* module scope makes the module encapsulated. 
* It means all the variable that are not explicitly exported from the module remain private to their own module and can't be accessed from outside. 

module1.js
```js
  const x = 10
  
  export const exptVar = 20
  
  const fun = function(){
    console.log("Anno. Function")
  }

```

module2.js
```js
import '.module1.js'

console.log(x) // reference error

console.log(exptVar) // 20

```

## **Lexical Environment**

A lexical environment is a collection of its local variables and lexical env of parent.

A lexical environment can be understood as a data structure that holds identfier-variable mappings.

**identifier** - name of variables,function and lexical environment of parent

**variable** - reference to actual objects

**Example**

```js

  var g = 10

  function outer(){
    var o = 10

    function inner(){
      var i = 10;
      console.log(g)
    }
    inner();
    
  }

  outer();

```  

  <div class = "flex" style = "background:transparent;border:10px solid yellow;border-top:none;margin:50px;" >
    <h2> <b>Call Stack</b> </h2>
    <div class="box" style = "background:red; " >
  <pre class="pre">
      inner() Execution context
      inner_lexical_env
      <pre class="pre">
  i : 10 
  parent-lexical : outer_lexical_env
      </pre>
  </pre>

  </div>
    <div class="box" style = "background:green; ">
    <pre class="pre">
      outer() Execution context
      outer_lexical_env
    <pre class="pre">
o : 10 
parent-lexical : global_lexical_env
  </pre>

  </pre>
    </div>
<div class="box" style = "background:blue; ">
  <pre class="pre" >
    global() Execution context
    global_lexical_env
    <pre class="pre" >
    g : 10 
    this : window
    parent-lexical : null
    </pre>
  </pre>
  </div>
  </div>


##  **Scope Chain**

*The Scope Chain is the hierarchy of scopes that will be searched in order to find a function or variable.*

In above example, in `inner()` function, `g` is not known to local scope , so it switches to its parent lexical environment ( technically where that function lexically present => `outer()`). Also `g` is not present in `outer()` so again search happend in `outer()`  parent lexical environment => `global()`.finally `g` found in global scope and get logged into the console. This whole process of searching the actual reference of variable name is called `scope chaining`.


## **Hoisting**
Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution.

Hoisting is a process whereby we can  safely access function before their declaration in the code.

Hoisting works with variables too, but we can't access the initialized value of that variable because <i style="color:aqua">JS only hoists decalaration not initialization</i>

#### <i style="color:violet"> **Hoisting with let & const** </i>
Hoisting  works with let and const too , but we can't access their value before their temporal dead zone.

**TDZ-(temporal dead zone)** : 
The TDZ starts at the **beginning of the variable's enclosing scope** ( *in case of function, it is from open bracket of function* )  and ends when it is **initialized**. Accessing the variable in this TDZ throws a ReferenceError.

The term "temporal" is used because the zone depends on the order of execution (time) rather than the order in which the code is written (position).

Example
```js
/* 
  Function Hoisting
*/

console.log(fun) // function fun() {...} -- hoisting works
console.log(fun2)  // undefined , as fun2 is decalared with undefined during creating phase
fun() // hello  -- hoisting works
fun2() // error , undefined can't be called  -- hoisting fails
 
function fun(){
  console.log("hello");
}

var fun2 = function(){
  console.log("declared function");
}


/*
  Variable Hoisting
*/

console.log(num); // Returns 'undefined' from hoisted var declaration (not 6)

var num; // Declaration

num = 6; // Initialization

console.log(num); // Returns 6 after the line with initialization is executed.




/*
  Hoisting with let and const
*/

// TDZ starts from first line of this file i.e start of  global scope

console.log(a) // Reference error, as `a` can't be accessed in its TDZ

let a ;  // Declared and Initialized with undefined -- TDZ ended here

console.log(a); // undefined  -- now we can access a, since it is declared now, but still we can't access its value as initialization is not done yet.

a = 10; // initialized with 10



function fun(){
 // TDZ for local starts from here
 
 let local; // TDZ for local ended here

 local = 2;

}

fun();

```

## [Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
A closure is the combination of a function and the lexical environment within which that function was declared

In other words, a closure gives you access to an outer function’s scope from an inner function. In JavaScript, closures are created every time a function is created.

Example:
```js

/* 
  Here inner() function forms as closure of its local environment and lexical environment. It means , it  store the reference to its lexical environnment objects also.  
*/
function outer(){
  var x = 10;

  function inner(){
    var y = 30;
  }

}

function outer2(){
  var x = 3;
  
  return function inner2(){
    var y = 4;
    return x + y;
  }
}


// Here, outer2() return the inner2()  function with its closure.
var getInner = outer2()
getInner() // 4 + 3 = 7


function func(x){
  return function (y){
    return x + y;
  }
}

var firstClosure = func(3)
var secondClosure = func(1)

firstClosure(3) // 3 + 3 = 6
secondClosure(2) // 1 + 2 = 3

```

<img src="assets/closure.PNG" style = "margin :10px 5px;" />


```js
function outer(){
    var x = 4;
    
    function inner(){
        var y = 5;
        console.log(x+y)

        // core will return along with the reference to its lexical environment (x,y)
        function core(){
            var z = 3;
            console.log(x+y+z)
        }
        core()
    }
    inner()
}

outer()

```
## Closure Scope Chain
```js

var e = 10;
function sum(a){ // forms closure with global env
  return  function outer(b){ // forms closure with sum() lexical env
    return function inner(c){ // form closure with sum() and inner() lexical env
      return function core(d){ // forms closure with inner(), outer() and sum() lexical env
        return a + b + c + d + e;
      }
    }
  }
}

/*
  sum(1) -> outer(2)-> inner(3) -> core(4)  => 1 + 2 + 3 + 4 = 10
  
  note : 
  sum() = outer
  sum()() = outer() = inner
  sum()()() = outer()() = inner() = core
  sum()()()() = outer()()() = inner()() = core() 

*/
console.log(sum(1)(2)(3)(4)); // log 20

```
[>> Checkout for more insight](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures#creating_closures_in_loops_a_common_mistake) 



# <h2 name = "functions"> **[Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions)** </h2>
  In JavaScript, functions are ```first-class objects``` , because they can have properties and methods just like any other object but unlike other objects they can be called. 
  
  *[function declaration, definition and expression](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/function#:~:text=The%20main%20difference%20between%20a,soon%20as%20it%20is%20defined.)*

  ### Declaration / Statement
  ```js
  //declaration
  function fun(){
    //statements
  }

  hoisted();

  function hoisted(){
    // It will work
  }

  ```
  * Can be hoisted ( i.e can be called before it's declaration)
  * function name can't change

  ### Expression
  ```js
    var funVar = function() {
    }

    // function expression with named declaration
    var funVar2 = function namedFun() {

    }

    console.log(funVar2.name) // namedFun

    var newName = funVar2; // namedFun( ) can be called using newName
    
  
    console.log(notHoisted) // undefined
    
    /* 
      even though the variable name is hoisted, the **definition** isn't. so it's undefined.
    */
    notHoisted(); // TypeError: notHoisted is not a function

    var notHoisted = function() {
      console.log('bar');
    };

  ```
  * **Can't be hoisted**
  * function name can be changed
  
  
  <details open >
  <summary>Properties</summary>  
  <ol>
  <li>
    function is Function class object
  </li>
  <li>
    function in JS are <i style = "color:yellow" >first class function</i> because of their ability to be used as value or decalre as a variable.
  </li>
  <li>
    Default return value is undefined.
  </li>

  <li>
  Function expressions are not hoisted onto the beginning of the scope, therefore they cannot be used before they appear in the code
  </li>
  
  </ol>
</details>


  *[Difference between Function constructor and function declaration](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function)*
    
  Every JS *function*  is the oject of  **Function** class.
  ```js

    var obj = function(){ }
    obj.constructor === Function // True

  ```
  The major difference between function and Function is that,
  
  * **Functions** created with the **Function constructor** do not create closures to their creation contexts; they always are created in the global scope.   
  When running them, they will only be able to access their own local variables and global ones, not the ones from the scope in which the Function constructor was created.
  
    * ```js
        var x = 20; // global
        function createFun(){
            var x = 10;
             
            return new Function( `
            var a = 20; 
            console.log(x) 
            ` )
        }
        var func = createFun();
        func(); // 20

      ```
  ### **[Anonymous functions](https://www.javascripttutorial.net/javascript-anonymous-functions/)**

  * functions without names
  * can  be used as argument to other function or for IIFE
  * Example

    ```js
     // Note that if you don’t place the anonymous function inside the (), you’ll get a syntax error. The () makes the anonymous function an expression that returns a function object.
    
    (function(){ })

    // IIFE
    (function(){
      console.log("I will be called immediately")
    })();


    // As a argument
    function checkNumber( number, fun ){
      console.log(fun(number)) ;
    }

    checkNumber(2, function (num){
      return num%2?"odd":"even";
    })

    ```


  ### **[Higher-order functions]()**
  A function that uses another function as an input argument or returns a function (HOF) is known as a higher-order function






# <h2 name = "oops"> **OOPS** </h2>
    

  ## Hello Objects 🙋‍♂️



# <h2 name = "concepts"> **Concepts** </h2>


  ## **[Prototype](#prototype)**
    
  Prototypes are the mechanism by which JavaScript objects inherit features from one another.
  


## **[IIFE]()**
  An IIFE (Immediately Invoked Function Expression) is a JavaScript function that runs as soon as it is defined
  ```js
    (function () {
    //statements
    })();


    var a = 10;

    ( named = function(){
      var a = 3;
      console.log(a)
    })();

  ```
  ### Use cases

