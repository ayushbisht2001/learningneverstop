## Mangling in python
 
Lets understand this concept with the given example,
 
```python
  class Parent:
 
    def __init__(self):
      self.name = "ayush"
      self.__age = 21
   
    def get_name(self):
      return self.name
   
    def __get_age(self):
      return self.__age
 
 
  obj = Parent()
 
  print(obj.get_name())
  print(obj.__get_age())
  print(obj.__age)
```
 
The above code seems to be syntactically correct , but on executing  the following error will occur.
 
```python
AttributeError: 'Parent' object has no attribute '__get_age'
```
 
So, why the above code is giving **AttributeError** ?  
_The answer is simple, this is because of **name mangling** in python._
 
The answer to this problem is in the problem itself. Since AttributeError Raised when an attribute reference or assignment fails.
 
Let's explore the problem a little bit more, by analyzing the list of names in the _**Parent**_ class scope. for that, we can use **dir()**
 
```python
print(dir(obj))
 
['_Parent__age', '_Parent__get_age', '__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'get_name', 'name']
 
```
Here we have two attributes ( '_Parent__age',  '_Parent__get_age' ), whose name are implicitly merged with the class name and in other words we can say the attribute name are mangled with the class name.

Since, there is no references for the **__age** and **__get_age** identifiers in the Parent class scope, hence causes AttributeError.
 
## What is Mangling ?
Name Mangling is nothing but the mechanism, by which any identifiers with 2 leading underscores ( like **__age**) is textually replaced with **_classname__age**.  
 
Till now, we have considered this mechanism as a problem, which restricts the naming of identifiers. However, in the actual scenario, it is helpful in some situation.
 
Let's discuss some cases where the name mangling is used.
 
- Sometimes, mangling can be considered  a way to implement private members in python.  
In the above example, we have seen that  AttributeError is raised, when we are trying to access the mangled variables outside the class however inside the class the variables are referenced with the same name.  
But there is a catch, we can access these private member by referencing the attributes with the mangled name.
  ```python
  print(obj._Parent__age)
  ```
  So, no perfect privacy in python ... 😩😩😩
 
- **Overriding** : Overriding is the ability of OOPs that allows the subclass to override the method of a parent class. The method in the subclass  is of the same name and signature as the super class method.

  ```python
    class Parent:
 
      def __init__(self):
        self.name = "ayush"
        self.age = 21
     
      def get_name(self):
        return self.name
     
      def get_age(self):
        return f"Parent class -  {self.age}"
 
    __get_age = get_age # private copy of original get_age method
 
  class Child(Parent):
 
      def __init__(self):
        self.name = "abc"
        self.age = 23
     
      def get_name(self):
        return self.name
     
      def get_age(self):
        return f"Child class -  {self.age}"
     
  child = Child()
 
  print(child.get_age()) # Child class - 23
  print(child._Parent__get_age())  # Parent class - 23
  ```
 
  As it is clear from the above example, Name mangling is helpful for letting subclasses override methods without breaking intraclass method calls.
 
 
 
### **Conclusion**
The conclusion from the above discussion is that implicit conversion of a variable (like __var ) name into a class mangled name is the inbuilt feature of python. Mangled variables can be used in various situations as discussed above.   
 
 


