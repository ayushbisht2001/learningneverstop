def check(func):
    def trace(*args, **kwargs):

        print("wait I'm checking something")
        if type(args[0]) is int:
            func(*args, **kwargs)
        else:
            print("Invalid type")
    return trace

@check
def fun(a):
    print("hello there", a)

fun("34")


# context manager

class C:

    def __init__(self, a, b):
        self.a = a
        self.b = b
    def cal(self):
        return self.a ** self.b


class A:

    def __init__(self, a, b):
        self.a = a
        self.b = b
    def calcul(self):
        return self.a + self.b


class B(A, C):
    def __init__(self, a, b):

        super().__init__(a, b)
    def calculation(self):
        return self.a * self.b

    def _private(self):
        return self.a

b = B(2,3)
print(b.cal())


def generator():
    for i in range(0, 10):
        yield i


a = generator()

print(next(a))
print(next(a))
print(next(a))




class ContextManager:

    def __init__(self, s) -> None:
        self.lis = list(s)

    def __enter__(self, *args, **kwargs):
        dis = { "value" : self.lis}
        return dis

    def __exit__(self, *args, **kwargs):
        return 



with ContextManager("hello") as cx:
    print(cx.get("value"))
