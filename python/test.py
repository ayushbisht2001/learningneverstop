class Parent:
  
    def __init__(self):
      self.name = "ayush"
      self.age = 21
    
    def get_name(self):
      return self.name
    
    def get_age(self):
      return f"Parent class -  {self.age}"

    __get_age = get_age

class Child(Parent):
    def __init__(self):
      self.name = "abc"
      self.age = 23
    
    def get_name(self):
      return self.name
    
    def get_age(self):
      return f"Child class -  {self.age}"
    
child = Child()

print(child.get_age())
print(child._Parent__get_age())