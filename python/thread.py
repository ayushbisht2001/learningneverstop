import threading
import time



def exe_func():
    print("thread starts exeucting")
    time.sleep(10)
    print("thread finished executing")


""" Without Threading"""

print( "*"*20 , "Without threading",  "*"*20 )

start_t = time.perf_counter()
exe_func()
exe_func()
exe_func()
end_t = time.perf_counter()

print( f" without threading , ends in {end_t - start_t}")



""" 
    With Threading

here, when t1 starts execute it get hold on ( sleep ) and the program control will then given to t2 ( when CPU become IDLE ) again same and control will be given to t3. ALl these threads
are on sleep and once wake up nearly at same time, since they were put on hold at same moment.

"""

print( "*"*20 , "With threading",  "*"*20 )

t1 = threading.Thread(target = exe_func)
t2 = threading.Thread(target= exe_func)
t3 = threading.Thread(target= exe_func)

start_t = time.perf_counter()
t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()

end_t = time.perf_counter()
print( f" with threading , ends in {end_t - start_t}")

