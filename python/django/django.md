# Django Restfull API


## Serializers methods in DRF


### Generic Serializer field
    Referencing with source attribute
    **Source** attrtibute can be used to refer
```python

    #serializers.py
    class ListingSerializer(serializers.ModelSerializer):
    ...
        popularity = serializers.ReadOnlyField(source='popularity')

    # models.py
    class Listing(models.Model):
        . . .

        @property
        def popularity(self):
            likes = self.post.likes.count
            return likes

```


### SerializerMethodField()
     It gets its value by calling a method on the serializer class it is attached to.

```python 
SerializerMethodField(method_name=None)
```


```python 

# Example 

class ContestantsListSerializers(serializers.ModelSerializer):

score = serializers.SerializerMethodField()
class Meta:
    model = Contestants
    fields = '__all__'    
    
    # fields = ('id', 'name', 'branch', 'year', 'created_on')    

def get_score(self, obj):
    event_id = self.context.get("event_id")
    sc = Score.objects.filter(event__id = event_id, participants__id = obj.id)
    if sc.exists():
        return sc.first().score

    return 0

```







## DO U KNOW ? 
### Passing extra info to serializers class ?
we can pass extra kwargs to serializer class using context parameter

ref : [stackoverflow](https://stackoverflow.com/questions/22988878/pass-extra-arguments-to-serializer-class-in-django-rest-framework) 

```python
    my_objects = MyModelSerializer(
        input_collection, 
        many=True, 
        context={'user_id': request.user.id}
    ).data

class MyModelSerializer(serializers.ModelSerializer):
...

    is_my_object = serializers.SerializerMethodField('_is_my_find')
...

    def _is_my_find(self, obj):
        user_id = self.context.get("user_id")
        if user_id:
            return user_id in obj.my_objects.values_list("user_id", flat=True)
        return False
...

```