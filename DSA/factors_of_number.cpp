#include<bits/stdc++.h>
using namespace std;

#define ll long long 


ll get_factors( ll n, vector<ll>&factor}){

    // Get all factors in O(sqrt(n)) TC

    ll _count = 0;

    for(ll i=1; i<=sqrt(n); i++){
        
        if( n%i == 0)
        if(i == n/i ){
            factor.push_back(i);
            _count += 1;
        }else
        {
            factor.push_back(i);
            factor.push_back(n/i);
            _count += 2;
        }
    }

    return _count;
}


ll count_factors( ll n){

    // Get all factors in O(sqrt(n)) TC

    ll _count = 0;

    for(ll i=1; i<=sqrt(n); i++){
        
        if( n%i == 0)
        if(i == n/i ){
            _count += 1;
        }else
        {
            _count += 2;
        }
    }

    return _count;
}


int main(){

    

    ll n;

    cin>>n;
    vector<ll> ans;
    cout<<"Total factors : " << count_factors(n);


    return 0;
}